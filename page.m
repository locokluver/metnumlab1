function [G, P] = page(A, p)

%Tenemos la matriz A de adyacencias, la convertimos a una matriz de Google
n = columns(A);

%Factor de teletransportacion
%p = 0.85;

%Hacemos que las columnas sumen 1
for j=1:n
  suma = 0;
	
  for i=1:n
		suma = A(i, j) + suma;
    G(i,j) = 0;
    P(i,j) = 0;
	end;
    
  if suma == 0
    G(:, j) = 1/n;
  else
    G(:, j) = p*A(:, j)/suma + (1-p)/n; 
  end;
  
  if suma == 0
    P(:, j) = 1/n;
  else
    P(:, j) = A(:, j)/suma; 
  end;
  
end;
return;