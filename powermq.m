function [lambda, x, iter] = powermq(A, z0, tol, nmax)
% POWERM Metodo de las Potencias
% Computa el valor propio LAMBDA de la matriz A, y el vector propio X 
% correspondiente con norma 1.
% TOL especifica la tolerancia del metodo.
% NMAX especifica la cantidad maxima de iteraciones. 
% Z0 especifica el vector inicial estimativo. 
% ITER es el numero de iteracion en el cual se computa X.

%Normalizamos el vector z0
q=z0/norm(z0); 
q2=q;

relres=tol+1;

%Iteracion 0
iter=0; 

%Vector z
z=A*q;

while relres>=tol & iter<=nmax
	q=z/norm(z); z=A*q;
	lambda=q'*z; 
    
    %Hacemos el metodo de potencia para el vector propio izquierdo
    x=q;
    z2=q2'*A; q2=z2/norm(z2); 
    q2=q2';	y1=q2;
    
    %Hacemos el producto de los vectores propios
    productolr=abs(y1'*x);
	
    iter=iter+1;
	relres=norm(z-lambda*q)/productolr;
	end
end
return;