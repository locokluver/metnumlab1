## Author: Laureano Kluver <lkluver@laureanok.local>
## Created: 2015-09-25

function plotJacobiPower (n, Mat, p, tol, kmax)

    MatReducida = Mat(1:n, 1:n);
    [G, P] = page(MatReducida, p);

    z0 = ones(n,1) / n;

    tic ();
    [JacobiLambda, JacobiResult, JacobiIterCount, JacobiResiduo ] = jacobi(P, p, tol, kmax);
    tJacobi = toc();
    tic();
    [PowerLambda, PowerResult, PowerIterCount, PowerResiduo] = power(G, z0, tol, kmax);
    tPower = toc();

    graphics_toolkit('gnuplot');
    
    clf reset;

		file = cstrcat("ResiduosJacobiPotencia", "N");
		file = cstrcat(file, num2str(n));
		file = cstrcat(file, "P");
		file = cstrcat(file, num2str(p));
		file = cstrcat(file, ".eps");
		
		description = cstrcat("Graficas de Residuos N=", num2str(n));
		description = cstrcat(description, " alpha=");
		description = cstrcat(description, num2str(p));
		description = cstrcat(description, " Tol=");
		description = cstrcat(description, num2str(tol));
    description = cstrcat(description, "\n tJacobi=");
    description = cstrcat(description, num2str(tJacobi));
    description = cstrcat(description, " tPotencia=");
    description = cstrcat(description, num2str(tPower));
		
    plot(JacobiResiduo, "r", "linewidth", 1, PowerResiduo, "linewidth", 1);
    grid minor on;
		
		title(description);
    legend (" Jacobi ", " Potencia ")
		xlabel("Iteraciones");
    ylabel("Residuo");
		
		hold on;
		
		print(file,'-deps');


endfunction
