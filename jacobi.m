function [lambda, z, iter, iterResiduo] = jacobi(P, p, tol, kmax)
        
        norm = @(x)sum(abs(x));
        n = columns(P);
        e = ones(n,1);

        v = e*((1-p)/n);      
        v = v/sum(v);

        matriz = p*P;
        vector = matriz*v;
        
        %Guardo el residuo
        iterResiduo(1) = norm((vector + v) - v);
        
        lambda = 1;
        for iter = 2:kmax        
          vectorNuevo = vector + v;    %x1
          vector = matriz*vectorNuevo;    
          
          res = norm((vector + v) - vectorNuevo);
          
          %Guardo el residuo
          iterResiduo(iter) = res;
          
          if(res < tol)
            break ; 
          end
        end
        z = vectorNuevo/sum(vectorNuevo);