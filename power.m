function [lambda, z, iter, iterResiduo] = power(A, z0, tol, kmax)
%function [lambda, x, iter] = power(A, z0, tol, kmax)
norm = @(x)sum(abs(x));
%Normalizamos el vector z0
q=z0/norm(z0);

%Inicializamos el residuo como la tolerancia + 1
relres=tol+1;

%Iteracion 0
iter=0; 

%Vector z
z=A*q;

%lambda es siempre 1
lambda=1;



%Mientras el ultimo residuo sea mayor o igual a la tolerancia
% y aun tengamos iteraciones permitidas
while relres>=tol & iter<kmax
	iter = iter + 1;
	%Normalizamos el vector q y lo multiplicamos por A
	q=z/norm(z); z=A*q;
    
    %Avanzamos en la iteracion que obtiene lambda
    %lambda=q'*z;    
   	relres=norm(z - lambda*q);
    
    iterResiduo(iter) = relres;
	end
end
return;