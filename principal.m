%Programa Principal

load harvard500.mat;
A = page(G);
n = rows(A);
z0 = ones(n, 1);

tolerancia = 0.00001;
itermax = 1000;

%Metodo de las Potencias de Quarteroni
[lambdaq, x1q, iterq] = powermq(A, z0, tolerancia, itermax);

%M�todo de las Potencias
[lambda, x1, iter] = power(A, z0, tolerancia, itermax);
